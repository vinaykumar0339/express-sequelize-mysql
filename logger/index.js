const { createLogger, format, transports } = require('winston');

module.exports = createLogger({
    format: format.combine(
        format.simple(),
        format.timestamp(),
        format.printf(info => `[[${info.timestamp}] - ${info.level} - ${info.message}`)),

    transports: [
        new transports.File({
            filename: `${__dirname}/combined.log`,
            level: 'info'
        }),
        new transports.File({
            filename: `${__dirname}/errors.log`,
            level: 'error'
        }),
        new transports.Console({
            level: 'debug',
        }),
    ]
})

