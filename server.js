const express = require('express');

const db = require('./app/database/connection');
const logger = require('./logger');

const uploadData = require('./app/routes/uploadData');
const movies = require('./app/routes/movies');
const users = require('./app/routes/users');



const { PORT } = require('./app/database/config');

const app = express();

app.use(express.json());

// db.sequelize.sync();

// db.sequelize.sync({
//     force: true,
// }).then(() => {
//     console.log("Drop and re-sync db.");
// })

// app.use('/upload', uploadData);

app.use('/movies', movies);
app.use('/account', users);

app.use((req, res) => {
    const moviesRoutes = {
        GET: '/movies',
        GET_WITH_ID: '/movies/{id}',
        POST: '/movies',
        PUT: '/movies/{id}',
        DELETE: '/movies/{id}',
    }

    const userRoutes = {
        POST: '/account/register',
        POST_LOGIN: '/account/login',
        POST_REFRESH_TOKEN: '/account/refreshToken',
        DELETE_LOGOUT: '/account/logout'
    }

    res.status(404).json({ 
        message: "use below routes only",
        moviesRoutes,
        userRoutes
    })
    res.end();
})

app.listen(PORT, () => {
    logger.info(`listening on port ${PORT}`);
})