// const dotenv = require('dotenv');
// dotenv.config();

module.exports = {
    PORT: process.env.PORT || 3000,
    mysql: {
        host: process.env.DB_HOST || 'localhost',
        database: process.env.DB_DATABASE || 'express-sequelize-mysql',
        user: process.env.DB_USER || 'vinay',
        password: process.env.DB_PASSWORD || 'Vinayhema@0339',
        dialect : 'mysql'
    },
    ACCESS_SECRETE_TOKEN: process.env.ACCESS_SECRETE_TOKEN,
    ACCESS_SECRETE_REFRESH_TOKEN: process.env.ACCESS_SECRETE_REFRESH_TOKEN
}