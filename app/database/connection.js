const Sequelize = require('sequelize');
const config = require('./config');


const sequelize = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
    host: config.mysql.host,
    dialect: config.mysql.dialect,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

const db = {
    sequelize,
    Sequelize
};

db.directors = require('../models/Director')(sequelize, Sequelize);
db.actors = require('../models/Actor')(sequelize, Sequelize);
db.movies = require('../models/Movie')(sequelize, Sequelize);
db.genres = require('../models/Genre')(sequelize, Sequelize);
db.users = require('../models/User')(sequelize, Sequelize);
db.refreshToken = require('../models/RefreshToken')(sequelize, Sequelize);

// associate one-many relation-ship
db.directors.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.directors, { 
    foreignKey: "directorId",
    as: "director"
})

// associate one-many relation-ship
db.actors.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.actors, { 
    foreignKey: "actorId",
    as: "actor"
})

// associate one-many relation-ship
db.genres.hasMany(db.movies, { as: "movies"});
db.movies.belongsTo(db.genres, { 
    foreignKey: "genreId",
    as: "genre"
})

module.exports = db;