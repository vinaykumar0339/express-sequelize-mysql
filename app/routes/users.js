const express = require('express');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { ACCESS_SECRETE_TOKEN, ACCESS_SECRETE_REFRESH_TOKEN } = require('../database/config');

const { users: User, refreshToken: RefreshToken } = require('../database/connection');
const logger = require('../../logger');
const router = express.Router();

// user registration
router.post('/register', [
    check('username')
        .isLength({ min: 6 })
        .withMessage('username should be at least 6 characters'),
    check('username')
        .custom(async value => {
            const user = await User.findOne({
                where: {
                    username: value
                }
            })
            if (user) {
                throw new Error('User already registered');
            } else {
                return true;
            }

        }),
    check('password')
        .isLength({ min: 6 })
        .withMessage('password should have at least 6 characters'),
    check('password').custom((value, { req }) => {
        if (value !== req.body.confPassword) {
            throw new Error('Password confirmation is incorrect');
        } else {
            return true;
        }
    })
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(422).json(errors);
        res.end();
    } else {

        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);

            const userData = {
                username: req.body.username,
                password: hashedPassword
            }

            const user = await User.create(userData);

            res.status(200).json({
                message: "user created successfully",
            });
            res.end();
        } catch (err) {
            logger.error(err);
            res.status(500).json({
                message: "Internal server error",
                err: err
            });
            res.end();
        }
    }
})

// user login
router.post('/login', userAuthentication, async (req, res) => {
    try {
        const accessToken = jwt.sign({
            username: req.body.username,
        }, ACCESS_SECRETE_TOKEN, { expiresIn: '15s' });

        // refresh token
        const refreshToken = jwt.sign({
            username: req.body.username,
        }, ACCESS_SECRETE_REFRESH_TOKEN);

        const dat = await RefreshToken.create({
            token: refreshToken
        });

        res.status(200).json({
            message: "user logged in",
            accessToken: accessToken,
            refreshToken: refreshToken

        });
        res.end();
    } catch (err) {
        logger.error(err);
        res.status(500).json({
            message: "internal server error",
            err: err
        });
        res.end();
    }
})

router.post('/refreshToken', check('token', 'please enter a refresh token').not().isEmpty(), async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json(errors).end();

    try {
        const token = req.body.token;
        const refreshToken = await RefreshToken.findOne({
            where: {
                token: token
            }
        })

        if (refreshToken) {
            jwt.verify(refreshToken.token, ACCESS_SECRETE_REFRESH_TOKEN, (err, user) => {
                if (err) {
                    res.status(403).json({
                        message: "invalid token"
                    });
                    res.end();
                } else {
                    const accessToken = jwt.sign({
                        username: user.username
                    }, ACCESS_SECRETE_TOKEN, { expiresIn: '15s' });

                    res.status(200).json({ accessToken });
                    res.end();
                }
            });
        } else {
            res.status(403).json({
                message: "invalid token"
            });
            res.end();
        }
    } catch (err) {
        logger.error(err);
        res.status(500).json({
            message: "internal server error",
            err: err
        });
        res.end();
    }

})

router.delete('/logout',
    check('token', 'please enter a refresh token').not().isEmpty(),
    async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(400).json(errors).end();

            const token = req.body.token;
            const refreshToken = await RefreshToken.destroy({
                where: {
                    token: token
                }
            })
            if (refreshToken) {
                res.status(200).json({
                    message: "deleted user token"
                })
                res.end();
            } else {
                res.status(404).json({
                    message: "token not found"
                });
                res.end();
            }

        }catch (err) {
            logger.error(err);
            res.status(500).json({
                message: "internal server error",
                err:err
            });
            res.end();
        }
    })

// user authentication
async function userAuthentication(req, res, next) {
    try {
        const username = req.body.username;
        const password = req.body.password;

        const user = await User.findOne({
            where: {
                username: username,
            }
        })

        if (user) {
            const auth = await bcrypt.compare(password, user.password);
            if (auth) {
                next();
            } else {
                res.status(404).json({
                    message: "incorrect password"
                });
                res.end();
            }

        } else {
            res.status(404).json({
                message: "user not found"
            });
            res.end();
        }

    } catch (err) {
        res.status(500).json({
            message: "Internal server error",
            err: err
        })
        res.end();
    }
}

module.exports = router;