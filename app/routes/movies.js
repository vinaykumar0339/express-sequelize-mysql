const express = require('express');
const {
    directors: Director,
    genres: Genre,
    movies: Movie,
    actors: Actor,
} = require('../database/connection');
const jwt = require('jsonwebtoken');
const { ACCESS_SECRETE_TOKEN } = require('../database/config');
const logger = require('../../logger');

const router = express.Router();


// checking authorization
function checkAuthorization(req, res, next) {
    const bearer = req.headers['authorization'];
    const token = bearer && bearer.split(' ')[1];
    if (!(token == null)) {
        jwt.verify(token, ACCESS_SECRETE_TOKEN, (err, user) => {
            if (err) {
                res.status(403).json({
                    message: "invalid token"
                })
                res.end();
            } else {
                req.user = user;
                next();
            }
        });

    } else {
        res.status(401).json({
            message: "user not authorized",
        });
        res.end();
    }
}

router.get('/', checkAuthorization, async (req, res) => {

    try {
        const movies = await Movie.findAll({
            include: ['director', 'actor', 'genre']
        });
        if (movies) {
            res.status(200).json(movies);
            res.end();
        } else {
            res.status(404).json({
                message: 'No record'
            })
        }

    } catch (err) {
        logger.error(err);
        res.sendStatus(500);
        res.end();
    }


})

router.get('/:id([0-9]+)', checkAuthorization, async (req, res) => {

    try {
        const movie = await Movie.findByPk(req.params.id, {
            include: ['director', 'actor', 'genre']
        })

        if (movie) {
            res.status(200).json(movie);
            res.end();
        } else {
            res.status(404).json({
                message: "No record"
            })
            res.end();
        }

    } catch (err) {
        logger.error(err);
        res.sendStatus(500);
        res.end();
    }

})

router.post('/', checkAuthorization, async (req, res) => {
    const movieData = req.body;

    if (!(Object.keys(movieData).length > 0)) {
        res.status(400).json({
            message: "please send the data",
        })
        res.end();
        return
    }

    try {
        
        if (req.body.Director) {
            const director = await Director.findOne({
                where: {
                    name: req.body.Director
                }
            })
            if (director) {
                movieData["directorId"] = director.id
            } else {
                const directorCreated = await Director.create({
                    name: req.body.Director
                })
                console.log(directorCreated);
                movieData["directorId"] = directorCreated.id;
            }
        }

        if (req.body.Actor) {
            const actor = await Actor.findOne({
                where: {
                    name: req.body.Actor
                }
            })

            if (actor) {
                movieData["actorId"] = actor.id
            } else {
                const actorCreated = await Actor.create({
                    name: req.body.Actor
                })
                movieData["actorId"] = actorCreated.id;
            }
        }

        if (req.body.Genre) {
            const genre = await Genre.findOne({
                where: {
                    type: req.body.Genre
                }
            })

            if (genre) {
                movieData["genreId"] = genre.id
            } else {
                const genreCreated = await Genre.create({
                    name: req.body.Genre
                })
                movieData["genreId"] = genreCreated.id;
            }
        }

        const movie = await Movie.create(movieData);
        res.status(200).json(movie);
        res.end();


    } catch (err) {
        logger.error(err);
        res.sendStatus(500);
        res.end();
    }

})

router.put('/:id([0-9]+)', checkAuthorization, async (req, res) => {

    const movieData = req.body;

    if (!(Object.keys(movieData).length > 0)) {
        res.status(400).json({
            message: "please send the data",
        })
        res.end();
        return
    }

    try {

        if (req.body.Director) {
            const director = await Director.findOne({
                where: {
                    name: req.body.Director
                }
            })

            if (director) {
                movieData["directorId"] = director.id
            } else {
                const directorCreated = await Director.create({
                    name: req.body.Director
                })
                movieData["directorId"] = directorCreated.id;
            }
        }

        if (req.body.Actor) {
            const actor = await Actor.findOne({
                where: {
                    name: req.body.Actor
                }
            })

            if (actor) {
                movieData["actorId"] = actor.id
            } else {
                const actorCreated = await Actor.create({
                    name: req.body.Actor
                })
                movieData["actorId"] = actorCreated.id;
            }
        }

        if (req.body.Genre) {
            const genre = await Genre.findOne({
                where: {
                    type: req.body.Genre
                }
            })

            if (genre) {
                movieData["genreId"] = genre.id
            } else {
                const genreCreated = await Genre.create({
                    name: req.body.Genre
                })
                movieData["genreId"] = genreCreated.id;
            }
        }

        const success = await Movie.update(movieData, {
            where: {
                id: req.params.id
            }
        });

        if (success[0]) {
            res.status(200).json({
                message: `movie id: ${req.params.id} is updated successfully`
            });
            res.end();
        } else {
            res.status(404).json({
                message: `id: ${req.params.id} is not found`
            })
            res.end();
        }



    } catch (err) {
        logger.error(err);
        res.sendStatus(500);
        res.end();
    }

})

router.delete('/:id([0-9]+)', checkAuthorization, async (req, res) => {

    try {
        const effect = await Movie.destroy({
            where: {
                id: req.params.id
            }
        })
        if (effect) {
            res.status(200).json({
                message: `deleted id: ${req.params.id} successfully`,
            })
            res.end();
        } else {
            res.status(404).json({
                message: `id: ${req.params.id} is not found`
            })
            res.end();
        }
    } catch (err) {
        logger.error(err);
        res.sendStatus(500);
        res.end();
    }

})

module.exports = router;