module.exports = (sequelize, Sequelize) => {
    const RefreshToken = sequelize.define('refresh_token', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        token: Sequelize.TEXT()
    })
    return RefreshToken;
}
