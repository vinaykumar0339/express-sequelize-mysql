module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        username: {
            type: Sequelize.STRING,
            len: [6,20],
            allowNull: false,
            trim: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        }
    })
    return User;
}
