module.exports = (sequelize, Sequelize) => {
    const Director = sequelize.define('director', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        name: Sequelize.STRING(30)
    })
    return Director;
}
