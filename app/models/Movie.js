module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define('movie', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        Rank: Sequelize.INTEGER(11),
        Title: Sequelize.STRING(50),
        Description: Sequelize.TEXT,
        Runtime: Sequelize.INTEGER,
        Rating: Sequelize.DECIMAL(5,2),
        Metascore: Sequelize.INTEGER,
        Votes: Sequelize.INTEGER,
        Gross_Earning_in_Mil: Sequelize.DECIMAL(10,2),
        Year: Sequelize.DATEONLY
    })
    return Movie;
}
